# Bobs Game Archive

An archive of everything related to Robert Pelloni.

Bob's Game Reddit
https://www.reddit.com/r/bobsgame

Install ok on Steam
steam://install/529720

Original Reddit thread with Lana-chan's Archive
https://www.reddit.com/r/bobsgame/comments/ojubtt/could_someone_send_me_a_copy_of_the_puzzle_game

Old Bob's Game Github Repo Archive
https://archive.softwareheritage.org/browse/search/?q=https%3A%2F%2Fgithub.com%2Fbobsgame%2F

New Bob's Game Github Repo Archive
https://github.com/bobsgamed

Bob's Game Revival Discord Server
https://discord.gg/Uxz2HAxQbZ

Bob's Game Ouya Port
https://archive.org/details/ouya_com.bobsgame.bg_0.1