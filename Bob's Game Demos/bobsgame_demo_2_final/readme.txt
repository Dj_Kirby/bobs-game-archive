SOFTWARE COMES WITH NO WARRANTY. DONT DIE!!!

//-----------------------------------------------------------
Controls
//-----------------------------------------------------------

Joysticks are supported. Try it out!


WASD or Up/Down/Left/Right
Space = A
Left Shift = B
Z = Y
X = X
Q = L
E = R
Backspace or [ = SELECT
Enter or ] = START
F = FPS Meter



//-----------------------------------------------------------
Config
//-----------------------------------------------------------

Edit config.ini for...

Fullscreen mode

Easy mode

??


//-----------------------------------------------------------
VSync
//-----------------------------------------------------------

Make sure you have VSync enabled for your Graphics Card.
It might also help to have the newest drivers for your card.


ATI: Catalyst Control Center -> 3D -> Wait for vertical refresh -> ON or APPLICATION SPECIFIES

NVidia: Nvidia Control Panel -> Manage 3d settings -> Program Settings -> TLR -> Vertical sync ON

Intel: Display Control Panel -> Settings -> Advanced -> Intel Graphics -> Graphics Properties... -> OpenGL or 3D Settings -> Asynchronous Flip to OFF




Copyright 2009 Robert Pelloni