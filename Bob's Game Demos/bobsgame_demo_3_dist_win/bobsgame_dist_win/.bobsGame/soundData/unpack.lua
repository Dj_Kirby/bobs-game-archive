local f = io.open( "data", "r" )

local i = 1
local lastName

for line in f:lines() do
	if i % 2 == 1 then
		local name, data = string.match( line, "([^:]-)%:(.*)" )
		lastName = name

		os.execute( "echo " .. data .. " | base64 -D | iconv -f UTF-8 -t ISO-8859-1 | gzip -d" )

		print( "" )
	else
		os.remove( lastName )
		os.execute( "echo " .. line .. " | base64 -D | iconv -f UTF-8 -t ISO-8859-1 | gzip -d > " .. lastName .. ".ogg" )
	end

	i = i + 1
end
