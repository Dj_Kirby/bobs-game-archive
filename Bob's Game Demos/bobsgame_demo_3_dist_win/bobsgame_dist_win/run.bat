@echo off
set DATA=%CD%
cd jars
java -cp bobsgame.jar;TWL.jar;TWLEffects.jar;commons-codec-1.7.jar;commons-compress-1.5.jar;commons-io-2.4.jar;commons-net-3.2.jar;easing.jar;gson-2.2.3.jar;hq2x.jar;ibxm77.jar;jinput.jar;jorbis-0.0.17.jar;logback-classic-1.0.12.jar;logback-core-1.0.12.jar;lwjgl.jar;lwjgl_util.jar;lwjgl_util_applet.jar;lzma.jar;netty-3.6.5.Final.jar;plugin.jar;restfb-1.6.12.jar;slf4j-api-1.7.5.jar;xpp3-1.1.4c.jar;xz-1.2.jar -Djava.library.path=natives -Duser.home="%DATA%" com.bobsgame.ClientMain
cd ..
IF ERRORLEVEL 9009 GOTO NoJava
goto exit

:NoJava
echo.
echo You need to install Java to play bob's game!
echo You can download Java at https://www.java.com/en/download/
set /P c=Do you want to download Java? [Y/N] 
if /I "%c%" EQU "Y" goto InstallJava
if /I "%c%" EQU "N" goto exit
goto exit

:InstallJava
start http://www.oracle.com/technetwork/java/javase/downloads/java-se-jre-7-download-432155.html

:exit
echo.