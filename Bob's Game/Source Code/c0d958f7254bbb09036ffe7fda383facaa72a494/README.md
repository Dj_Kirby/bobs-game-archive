**bob's game** is "source available" and "free as in beer" but not "Open Source" and "free as in speech" (yet).

That means **no forks** except to create pull requests!

Also see:

https://github.com/bobsgame/bobsgame

https://github.com/bobsgame/bgEditor

https://github.com/bobsgame/BobsGameServer

https://github.com/bobsgame/BobSharedLibs

https://github.com/bobsgame/BobsServerIndex

https://github.com/bobsgame/BobsSTUNServer

https://github.com/bobsgame/BobNetLibs


--

# *bob's game* is:

## *THE GREATEST PUZZLE GAME EVER MADE*

## EVERY PUZZLE GAME IN ONE

## A CUSTOM PUZZLE GAME CREATION SYSTEM

## A WORLD TOURNAMENT PLATFORM AND GLOBAL RANKING AND LEADERBOARDS FOR ALL PUZZLE GAMES

## LOCAL MULTIPLAYER WITH UNLIMITED PLAYERS

## ONLINE MULTIPLAYER WITH UNLIMITED PLAYERS

## MIXED MODE ONLINE/LOCAL MULTIPLAYER

## COMPLETELY FREE

## WRITTEN FROM SCRATCH IN C++

## SOURCE CODE AVAILABLE

## DOWNLOAD IT RIGHT NOW!

## This game is actively developed. New features are being added constantly and bugs will happen.

[Development Forum](http://bobsgame.com/forum) is open for bug reports. Please report any issues you notice. I rely on your testing.

[http://www.bobcorporation.com](http://www.bobcorporation.com) (Main website)

[Pending Wikipedia article](https://en.wikipedia.org/w/index.php?title=Bob%27s_Game&oldid=713042467), help revise the current entry as I am not allowed to.

**bob's game** (from the infamous upcoming RPG *"bob's game"*) is a multiplayer puzzle game- but not just any puzzle game, **EVERY puzzle game!** It's a build-your-own puzzle game with a puzzle game creator where YOU define all the rules to your own custom game!

More than that, great care has been made to ensure that **bob's game** is the *best* version of every puzzle game. I went through every puzzle game forum and wiki I could find and made sure to implement every detail and feature required by tournament level hardcore players. It has wall kicks, floor kicks, spin kicks, T-spins, adjustable timing, powerups, you name it. All of these features apply to all the games however you want, making it the best version of every game!

**bob's game** change in real time as you play it, morphing from game to game and making every round completely unique. Or you can choose to play just your favorite game type against your friends with fantastic controls, super tight timing, and features and powerups from other games.

**bob's game** is completely customizable, allowing the player to create in-depth rulesets to mimic existing puzzle games or create entirely new ones to add to the online library.

Create your own puzzle modes! Invent custom rulesets or challenge your friends to a sequence of all your favorite games. Compete in an online world championship and improve your skills to reach the top of the public leaderboard.

**bob's game** is constantly evolving with new theme packs, new rules, and user-designed puzzle packs.

**bob's game** currently supports as many local or network players as you want, mixed local/network multiplayer, Facebook integration, game controller support, and more.

It will be continuously updated for Windows, Mac, Linux, web browser, Android, iPhone, and possibly Nintendo Switch and Playstation 4.

It will also have the source code available on GitHub so it can grow into a standard tournament platform for all puzzle games!

**bob's game is not done- it's quite playable, but I am actively adding many more features.**

## Upcoming feature list:
* Lots more game mode features, modes, and goals
* Better multiplayer lobby with chat
* Tournament features and ladders, regional and global
* Matchmaking by region or rank
* Server verification
* Spectator mode, replays, challenge replay
* Custom themes, sounds, and music
* Android, iOS, and Browser support

# License
Copyright 2017 BobCorporation, Robert Pelloni

bob's game® is a registered trademark of Robert Pelloni

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. You may not distribute the bob’s game source code or binary or produce any derivative work from its source code under another name. Redistributions of source code or binary form must be named bob's game.

4. You may not distribute or use the source code or binary for profit or income, including revenue from advertising, tournaments, etc.  All redistributions and uses must be provided completely free of charge. "Let's Play" videos or other streaming videos are exempt and may include advertising revenue.

5. You may not create and maintain a fork or unique redistribution of the bob’s game source code except for a GitHub fork for the purpose of submitting pull requests, not to be distributed independently.  The intent of the bob’s game source code release is so that its community may submit bug fixes and additions to the main project.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
